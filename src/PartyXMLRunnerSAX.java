import java.io.FileOutputStream;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class PartyXMLRunnerSAX {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			//Aufgabe 1: Read xml
			/*******************************************************/
			Document jdom = new SAXBuilder().build("factbook.xml");
			
			Element root = jdom.getRootElement();
			List<Element> guests = root.getChildren();
			Element albert = null;
			
			//2. Frist guest element
			/*******************************************************/
			for (Element element : guests) {
				if (element.getAttributeValue("name").equals("Albert")) {
						albert = element;
				}
			}
			//3. Drinks of Albert
			/*******************************************************/
			List<Element> drinks = albert.getChildren("drink");
			
			for (Element drink : drinks) {
				System.out.println(drink.getText());
			}
			
			Element martina = null;
			//4. Is Martina Single?
			/*******************************************************/
			for (Element element : guests) {
				if (element.getAttributeValue("name").equals("Martina")) {
						martina = element;
				}
			}
			
			Element status = martina.getChild("status");
			String singlestring = status.getAttributeValue("single").equals("false") ? "nicht " : ""; 
			System.out.println("Martina ist " + singlestring + "Single");
			
			java.util.Iterator<Element> it =  drinks.iterator();
			
			//5. Edit drink wine to water
			/*******************************************************/
			while (it.hasNext()) {
				Element next = it.next();
				if (next.getText().equals("wine"))
					next.setText("water");
				
			}
			
			for (Element drink : drinks) {
				System.out.println(drink.getText());
			}
			
			 //6. Martina and Martin fell in love
			/*******************************************************/
			for (Element guest : guests) {
				if (guest.getAttributeValue("name").equals("Martina")) {
					guest.getChild("status").setText("inLove");
				}
			}
			
			Element martin = new Element("guest");
			martin.setAttribute(new Attribute("name", "Martin"));
			Element state = new Element("status").setText("inLove");
			martin.setContent(state);
			root.addContent(martin);

			
			for (Element guest : guests) {
				System.out.println(guest.getAttributeValue("name"));
				if (guest.getChild("status") != null) {
					System.out.println(guest.getChildText("status"));
				}
			}
			
			FileWriter writer = new FileWriter("src/PartyOut.xml");
			XMLOutputter outputter = new XMLOutputter();
			outputter.setFormat(Format.getPrettyFormat());
			outputter.output(jdom, writer);
			
			
			
		} catch (JDOMException | IOException e) {
			e.printStackTrace();
		}
		
	}

}
