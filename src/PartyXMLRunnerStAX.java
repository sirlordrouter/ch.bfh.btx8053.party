import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;




public class PartyXMLRunnerStAX {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		XMLEventReader xmlReader;
		try {
			InputStream in = new FileInputStream("bin/Party.xml");
			xmlReader = XMLInputFactory.newInstance().createXMLEventReader(in);
			while(xmlReader.hasNext()) {
				XMLEvent event = xmlReader.nextEvent();
				switch (event.getEventType()) {
				case XMLStreamConstants.START_DOCUMENT:				
					break;
				case XMLStreamConstants.END_DOCUMENT:
					break;
				case XMLStreamConstants.START_ELEMENT:
					StartElement element = event.asStartElement();
					for (Iterator<?> a = element.getAttributes(); a.hasNext();) {
						Attribute att = (Attribute) a.next();
						System.out.println(att);
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					break;
				case XMLStreamConstants.CHARACTERS:
				default:
					break;
				}
			}
			
			
		} catch (XMLStreamException | FactoryConfigurationError | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

}
